const Git = require('simple-git')
const fs = require('fs-extra');


let newVersion = null
module.exports=async function fun(arg) {
  var GitPath = process.cwd()
  let a=await fs.pathExists(GitPath + '/package.json')
  if(!a){
    console.log('\x1b[91m','当前目录不是git项目','\033[0m')
    return ;
  }
  let version = require(GitPath + '/package.json').version
  Git(GitPath)
    .pull()
    .tags(function(err, tag) {
      if (err) {
        throw err
      }
      version=arg.p===''?version:'v'+version
      //console.log(arg,version)
      let a = tag.all.filter(i => {
        return i.indexOf(version.toString()) ===0
      })
      if (a.length == 0) {
        newVersion = version.toString() + '.0'
      }else{
        newVersion=version+"."+(Number(a[a.length-1].match(/\d+$/g)[0])+1)
      }
      //console.log(a, newVersion)
      if (newVersion) {
        Git(GitPath).addAnnotatedTag(newVersion, newVersion, function() {
          Git(GitPath).pushTags('origin', function() {
            console.log('============>',newVersion)
          })
        })
      }
    })
}
