const Git = require('simple-git');
const fs = require('fs-extra');
const chalk = require('chalk');
module.exports = function fun(args) {
  if (!args.d) {
    console.log(chalk.red('请输入新建名'));
    return;
  }
  var GitPath = process.cwd();
  var t = args.d;
  fs.emptyDirSync(t);
  t = process.cwd() + '/' + t;
  let f = args.f ? args.f : 'git@gitlab.com:wmxteam/devautojs.git';

  Git(GitPath).clone(f, t, {}, function(err) {
    if(err){
        throw err
    }else{
        console.log('\x1b[91m','------',t,'\033[0m')
    }
  });
};
